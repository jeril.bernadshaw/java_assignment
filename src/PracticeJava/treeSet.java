package PracticeJava;

import java.util.TreeSet;

public class treeSet {

	public static void main(String[] args) {
		TreeSet <Integer> tre = new TreeSet<Integer>();  
		  tre.add(2);     
		  tre.add(8);  
		  tre.add(5);  
		  tre.add(1);  
		  tre.add(10); 
		  
		  System.out.println("TreeSet: " +tre); 
		  System.out.println("Ceiling value of 3 in set: " +tre.ceiling(3));

	}

}
