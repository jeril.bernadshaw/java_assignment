package Stringss;

public class stringBuilder {

	public static void main(String[] args) {
		StringBuilder str = new StringBuilder("Java");
        System.out.println("String = "+ str.toString());
                        

        StringBuilder reverseStr = str.reverse();
 
        System.out.println("Reverse String = "+ reverseStr.toString());

        str.appendCodePoint(1);
 
        System.out.println("Modified StringBuilder = "+ str);
                         
        int capacity = str.capacity();
        
        System.out.println("StringBuilder = " + str);
        System.out.println("Capacity of StringBuilder = "  + capacity);

	}

}
