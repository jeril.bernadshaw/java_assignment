package PracticeJava;

import java.util.HashMap;

public class hashmap {
	public static void main(String[] args) {
		
		HashMap<String, Integer> map = new HashMap<>();
		 
        map.put("a", 10);
        map.put("b", 30);
        map.put("c", 20);
 
        System.out.println("Size of map is "+ map.size());
                                                                     
        System.out.println(map);
 
        if (map.containsKey("a")) {
 
            // Mapping
            Integer a = map.get("a");
 
                                                                
            System.out.println("value for key"+ "\"a\" is " + a);
        }
		
	}

}
