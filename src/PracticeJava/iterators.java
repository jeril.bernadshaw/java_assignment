package PracticeJava;

import java.util.ArrayList;
import java.util.Iterator;

public class iterators {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		System.out.println(list);
		
		Iterator<String> itr = list.iterator();
		while(itr.hasNext()) {
			Object fetch_value = itr.next();
			System.out.println(fetch_value +" ");
		}

	}

}
